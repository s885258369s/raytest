#!/bin/bash
#版本2:新增域名判斷是否存在提醒
#adic代表js所在
#dic是複製備份和修改出來的檔案
#請小心使用
 
adic="/data/LandingPage/setting/"
dic="/tmp/fred/"
 
dl=(
5qz.cc
5z59.com
6n25.com
8h81.com
8yg.cc
b6a2.com
dq1.cc
f5g.cc
ft2.cc
gw2.cc
)
 
jl=(
3687083
3687126
3687137
3687145
3687155
3687165
3687192
3687203
3687220
3687225
)
 
echo -n "輸入總共的欲添加數量組(一組就寫1)："
read num
num=`expr $num`
 
echo -n "輸入JS檔案名稱(例如shengda_84.js)："
read fin
 
now="$(date +'%m%d')"
echo "------------------------------------"
echo "----腳本開始也會檢查域名是否存在----"
echo "------------------------------------"
echo "----以下重複域名刪除時請小心操作----"

cp -a ${adic}"${fin}" ${dic}"${fin}" && cp -a ${dic}"${fin}" "${dic}"${fin}_${now}

echo "var extensions = {" >> ${dic}"${fin}"

for (( i=0 ; i<${num} ; i++ ));do
	ans_a=$(grep -r ${dl[$i]} ${dic}"${fin}" | awk  '{print $1}' | tr -d "':")
	if [ -n "$ans_a" ];
		then
			echo -e "\e[1;42m$ans_a "域名已重複添加,請手動刪除原有廣告鏈結"\e[0m"	
	fi
	echo "'${dl[$i]}': {" >> "${dic}"${fin}
	echo "  agent:'${jl[$i]}'," >> "${dic}"${fin}
	echo "}," >> "${dic}"${fin}

done
 
echo "};" >> "${dic}"${fin}
echo "------------------------------------"
echo "------------------------------------"
echo "------------------------------------"
echo "------------------------------------"

echo "新的設置檔已生成在: "${dic}"${fin}"
